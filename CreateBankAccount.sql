

drop table BankAccount;

CREATE TABLE BankAccount(
    account_no INT PRIMARY KEY,
	customer_id INT,
    balance DECIMAL(10, 2) DEFAULT 0.0,
	FOREIGN KEY (customer_id) REFERENCES Customers(customer_id),
    account_status VARCHAR(255) DEFAULT 'open'
);
select * from BankAccount;

