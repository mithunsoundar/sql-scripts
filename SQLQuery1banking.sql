
/*TASK 1 -Banking database setup */
--create Database name called 'Banking'
--Create Tables in Banking database 
CREATE TABLE Customers (
     customer_id INT PRIMARY KEY, 
     name VARCHAR(255),
     phone VARCHAR(20),
     email VARCHAR(255),
     gender VARCHAR(10),
     address VARCHAR(255),
 );

CREATE TABLE BankAccount(
    account_no INT PRIMARY KEY,
	customer_id INT,
    balance DECIMAL(10, 2) DEFAULT 0.0,
	FOREIGN KEY (customer_id) REFERENCES Customers(customer_id),
    account_status VARCHAR(255) DEFAULT 'open'
);

select * from BankAccount;

select * from Customers;
/*TASK 2 -Create/Open Account*/
--Create Customer table

-- insert into User into users table 
INSERT INTO Customers  ( customer_id,name, phone, email, gender, address)
VALUES (111,'Aishu', '9344586693', 'ishus@gmail.com', 'Male', '123 Main Street');

INSERT INTO Customers  ( customer_id,name, phone, email, gender, address)
VALUES (222,'Paari', '9344782829', 'Paaris@gmail.com', 'Male', '123 Main Street');

INSERT INTO Customers  (  customer_id,name, phone, email, gender, address)
VALUES (333,'Mithun', '123-456-7891', 'Mithuns@gmail.com', 'Male', '123 Main Street');

select * from Customers;

-- insert into Acounts table , including UserID as a forginkey
INSERT INTO BankAccount (account_no,customer_id,balance)
VALUES (101, 111, 1000.00);

INSERT INTO BankAccount (account_no,customer_id,balance)
VALUES (102, 222, 2000.00);

INSERT INTO BankAccount (account_no,customer_id,balance)
VALUES (103, 333, 3000.00);

INSERT INTO BankAccount (account_no,customer_id,balance)
VALUES (104, 444, 5000.00);

select * from BankAccount

/*TASK 3-Deposit*/
--Deposit Account
UPDATE BankAccount
SET balance = balance + 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');


UPDATE BankAccount
SET balance = balance + 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

select * from BankAccount;



/*TASK4-Withdrwal*/
--Withdrwal account

UPDATE BankAccount
SET balance = balance - 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

UPDATE BankAccount
SET balance = balance - 1500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');

select * from BankAccount;

/*Task5-Update Account*/
UPDATE Customers
SET
    gender = 'Female'
    WHERE
    name = 'Aishu';

UPDATE Customers
SET
    Phone= '987828288'
    WHERE
     name = 'Aishu';

/*Trasfer account*/
-- Debit 500 amount from the Mithun's account 
UPDATE BankAccount
SET balance = balance - 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

-- Credit 500 to Paari account 
UPDATE BankAccount
SET balance = balance + 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');

select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');
select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');
/*Task5-Close Account*/
--Close Account

UPDATE BankAccount
SET  ACCOUNT_STATUS= 'closed'
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');

--drop Table BankAccount 
--drop Table Customers















