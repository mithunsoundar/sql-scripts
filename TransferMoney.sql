/*Trasfer account*/
-- Debit 500 amount from the Mithun's account 
UPDATE BankAccount
SET balance = balance - 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

-- Credit 500 to Paari account 
UPDATE BankAccount
SET balance = balance + 500.00
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');

select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');
select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');
/*Task5-Close Account*/
--Close Account
UPDATE BankAccount
SET  ACCOUNT_STATUS= 'closed'
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');

