/* Cash withdraw from Paari amount 1500 on 2023-11-08*/
--------------------------------------
/* Transfer to amount 100 from Paari to Lavavanya on 2023-11-08*/

USE Banking;
DECLARE @variable_customer_id INT;
select  @variable_customer_id= customer_id from Customers where name='Paari';
Print @variable_customer_id;
DECLARE @variable_account_no INT;
select  @variable_account_no= account_no from BankAccount where customer_id=@variable_customer_id;
Print @variable_account_no;
DECLARE @variable_TransactionDate DATETIME = CONVERT(DATETIME, '2023-11-08');
PRINT @variable_TransactionDate;

DECLARE @TransDescription VARCHAR(25);
SET @TransDescription = 'Transfer Withdraw'

INSERT INTO BankTransactionTab(
	customer_id, 
	account_no, 
	TransactionDate,
	TransDescription,
	TransactionAmount)
VALUES(
	@variable_customer_id,
	@variable_account_no, 
	@variable_TransactionDate,
	@TransDescription,	
	100.00
);

/* deduct 100 from paari account and make a new entry in BankTransactionTab for Paari about this transation  */
UPDATE BankAccount
SET balance = balance - 100.00 from BankAccount
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

/* deposit  100 to Lavanya account and make a new entry in BankTransactionTab for Lavanya  about this transation  */
UPDATE BankAccount
SET balance = balance + 100.00 from BankAccount 
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Lavanya');

select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');
select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Lavanya');
select * from BankTransactionTab;

USE Banking;

DECLARE @variable_customer_id INT;
select  @variable_customer_id= customer_id from Customers where name='Mithun';
Print @variable_customer_id;
DECLARE @variable_account_no INT;
select  @variable_account_no= account_no from BankAccount where customer_id=@variable_customer_id;
Print @variable_account_no;
DECLARE @variable_TransactionDate DATETIME = CONVERT(DATETIME, '2023-11-08');
PRINT @variable_TransactionDate;

DECLARE @TransDescription VARCHAR(25);
SET @TransDescription = 'Transfer Withdraw'

INSERT INTO BankTransactionTab(
	customer_id, 
	account_no, 
	TransactionDate,
	TransDescription,
	TransactionAmount)
VALUES(
	@variable_customer_id,
	@variable_account_no, 
	@variable_TransactionDate,
	@TransDescription,	
	1000.00
);
--Select SET @balance=balance from BankAccount where account_no = @variable_account_no;
--UPDATE BankAccount SET balance = @balance - @amount WHERE account_no = 101;
/* deduct 100 from pari account and make a new entry in BankTransactionTab for Paari about this transation  */
UPDATE BankAccount
SET balance = balance - 100.00 from BankAccount
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');

/* deposit  100 to Lavanya account and make a new entry in BankTransactionTab for Lavanya  about this transation  */
UPDATE BankAccount
SET balance = balance + 100.00 from BankAccount 
WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Lavanya');

select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Mithun');
select * from BankAccount WHERE customer_id = (SELECT customer_id FROM Customers WHERE name = 'Paari');
select * from BankAccount WHERE customer