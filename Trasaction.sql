--Create table for bank transaction--
Create table BankTransactionTab
(
	Transaction_id int Primary Key IDENTITY(1,1),
	customer_id int FOREIGN KEY (customer_id) REFERENCES Customers(customer_id),
	account_no int FOREIGN KEY (account_no) REFERENCES BankAccount(account_no),
	TransactionDate datetime not null Default getdate(), 
	TransDescription varchar(25),	
	TransactionAmount money Default '0'	
);
select * from BankTransactionTab

-- Insert values into BankTransactionTable
INSERT INTO BankTransactionTab(Transaction_id, customer_id, account_no, TransactionDate, TransDescription, TransactionAmount)
VALUES
    (1, 111, 101, GETDATE(), 'Deposit', 1, 1000.00, 5000.00),
    (2, 222, 102, GETDATE(), 'Withdrawal', 2, 500.00, 4500.00);
	INSERT INTO BankTransactionTab(Transaction_id, customer_id, account_no, TransactionDate, TransDescription, TransactionAmount)
VALUES
	(3, 333, 103, GETDATE(), 'Withdrawal', 3, 1500.00, 1500.00);
    Select * from BankTransactionTab;
