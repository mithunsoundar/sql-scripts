

drop table Customers;
/*TASK 1 -Banking database setup */
--create Database name called 'Banking'
--Create Tables in Banking database 
CREATE TABLE Customers (
     customer_id INT PRIMARY KEY, 
     name VARCHAR(255),
     phone VARCHAR(20),
     email VARCHAR(255),
     gender VARCHAR(10),
     address VARCHAR(255),
 );
 
select * from Customers;